# Circular Gravity Ball
Made in Unity using C#.

Game jam at university, competitive multiplayer game with circular gravity.
Players can move around a sphere with circular gravity, jump, inverse their own gravity, and hit the ball.
Hitting the ball marks it as yours, and if the ball hits another player you gain a point. First player to 10 points wins.

## Team
### Eirik Hiis Hauge
- Game systems
- Circular gravity
- Player, map, and ball logic
- GUI
### Maarten Dijkstra
- Splitscreen
- 3D assets
- Menus

## Build
A build can be found as a .zip file in the Downloads section.

## Video Demo
https://youtu.be/2UAg00egswc

## Screenshots
![Screenshot1](Screenshot1.png)
![Screenshot2](Screenshot2.png)
