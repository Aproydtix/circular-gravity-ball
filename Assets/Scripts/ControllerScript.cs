﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

///The Controller Script is responsible for the game's structure, and does things like spawn players and keep the score
public class ControllerScript : MonoBehaviour {

    public int playerCount = 2; //Amount of players 1-4 (though 1-player is just silly)
    public GameObject ball;     //The ball object
    public GameObject[] players;//The players
    public int[] playerScore;   //The player scores
    public Color[] playerColor; //The player colors

    public float gravityIntensity;  //The gravity intensity
    public GameObject gravityCentre;//The gravity source
    public GameObject playerPrefab; //The player prefab
    public GameObject ballPrefab;   //The ball prefab

    public int winner = -1;                     //The winner, -1 until winner has been decided
    GameObject filter;                          //Filter overlay to darken screen

    // Use this for initialization
    void Start ()
    {
        Time.timeScale = 1f;                    //Timescale set to 1
        playerCount = GameObject.Find("UI").GetComponent<StartOptions>().players;
        players = new GameObject[playerCount];  //Initializes the array
        playerScore = new int[playerCount];     //Initializes the array

        for (int i = 0; i < playerCount; i++)   //Loops through players
        {
            players[i] = Instantiate            //Creates player object
                (
                playerPrefab,                   //Player prefab
                gravityCentre.transform.position + new Vector3( //Evenly spaces players around the centre
                    Mathf.Cos(Mathf.PI + Mathf.PI * (i+.001f) * 2 / (playerCount)),
                    Mathf.Sin(Mathf.PI + Mathf.PI * (i+.001f) * 2 / (playerCount)),
                    0) * 3f,                    //Distance from centre
                transform.rotation
                );
            players[i].name = "Player " + (i + 1).ToString();                       //Set player name
            players[i].GetComponent<PlayerScript>().player = i;                     //Set player id
            players[i].GetComponent<PlayerScript>().gravitySource = gravityCentre;  //Set gravity source
            players[i].GetComponent<PlayerScript>().controllerScript = this;        //Set controller script
        }

        if (playerCount == 1)
        {
            players[0].GetComponentInChildren<Camera>().rect = new Rect(0f, 0f, 1f, 1f);
        }
        if (playerCount == 2)
        {
            players[0].GetComponentInChildren<Camera>().rect = new Rect(0f, 0f, 0.5f, 1f);
            players[1].GetComponentInChildren<Camera>().rect = new Rect(0.5f, 0f, 0.5f, 1f);
        }
        if (playerCount == 3)
        {
            players[0].GetComponentInChildren<Camera>().rect = new Rect(0f, 0f, 1 / 3f, 1f);
            players[1].GetComponentInChildren<Camera>().rect = new Rect(1 / 3f, 0f, 1 / 3f, 1f);
            players[2].GetComponentInChildren<Camera>().rect = new Rect(2 / 3f, 0f, 1 / 3f, 1f);
        }
        if (playerCount == 4)
        {
            players[0].GetComponentInChildren<Camera>().rect = new Rect(0f, 0.5f, 0.5f, 0.5f);
            players[1].GetComponentInChildren<Camera>().rect = new Rect(0.5f, 0.5f, 0.5f, 0.5f);
            players[2].GetComponentInChildren<Camera>().rect = new Rect(0f, 0f, 0.5f, 0.5f);
            players[3].GetComponentInChildren<Camera>().rect = new Rect(0.5f, 0f, 0.5f, 0.5f);
        }

        float ranDir = Random.Range(0f, Mathf.PI * 2f);             //Random angle in radians
        ball = Instantiate                                          //Create ball
            (
            ballPrefab,                                             //Ball prefab
            gravityCentre.transform.position + new Vector3( 
                    Mathf.Cos(ranDir),
                    Mathf.Sin(ranDir),
                    0) * 3.5f,                                      //Distance from centre
            transform.rotation
            );
        ball.GetComponent<BallScript>().controllerScript = this;    //Set controller script

        filter = new GameObject();                                  //Create filter
        filter.name = "Filter";                                     //Set name
        filter.transform.position = new Vector3(0, 0, -5);          //Set position
        filter.transform.localScale = Vector3.one * 1000;           //Set scale
        filter.AddComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Pixel"); //Set sprite
        filter.GetComponent<SpriteRenderer>().material.color = Color.clear; //Set color to clear
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Return))   //if Enter is pressed
        {
            SceneManager.LoadScene(0);          //Reload Main Menu
        }

        for (int i = 0; i < playerCount; i++)   //Loop through players
        {
            if (playerScore[i] >= 10)           //if player's score above 10
            {
                winner = i;                     //Set winner to this player
                Time.timeScale = 0f;            //Stop time
                filter.GetComponent<SpriteRenderer>().material.color = new Color(0, 0, 0, .5f); //Set filter to transparent black
            }
        }
    }

    private void OnGUI()
    {
        int w = Screen.width;
        int h = Screen.height;

        Rect[] playerRect = new Rect[4];
        playerRect[0] = new Rect(-w/2.5f, -h/2.5f, w, h);
        playerRect[1] = new Rect(w/2.5f, -h/2.5f, w, h);
        playerRect[2] = new Rect(-w/2.5f, h/2.5f, w, h);
        playerRect[3] = new Rect(w/2.5f, h/2.5f, w, h);
        for (int i = 0; i < playerCount; i++)
        {
            GUIStyle score = new GUIStyle();
            score.fontSize = 64;
            score.normal.textColor = playerColor[i];
            score.alignment = TextAnchor.MiddleCenter;
            GUI.Label(playerRect[i], playerScore[i].ToString(), score);
        }

        if (winner >= 0)
        {
            GUIStyle playerWin = new GUIStyle();
            playerWin.fontSize = 128;
            playerWin.normal.textColor = playerColor[winner];
            playerWin.alignment = TextAnchor.MiddleCenter;
            GUIStyle wins = new GUIStyle();
            wins.fontSize = 128;
            wins.normal.textColor = Color.white;
            wins.alignment = TextAnchor.MiddleCenter;
            GUIStyle restart = new GUIStyle();
            restart.fontSize = 64;
            restart.normal.textColor = Color.white;
            restart.alignment = TextAnchor.MiddleCenter;
            GUI.Label(new Rect(0, 0, w, h), "Player " + (winner + 1).ToString() + "\n\n", playerWin);
            GUI.Label(new Rect(0, 0, w, h), "\nWins!\n", wins);
            GUI.Label(new Rect(0, 0, w, h), "\n\n\n\nPress Enter to restart", restart);
        }
    }
}
