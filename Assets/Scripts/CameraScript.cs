﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {
    	
	// Update is called once per frame
	void Update () {
        transform.localEulerAngles = new Vector3
            (
            0,
            0,
            ((transform.parent.GetComponent<PlayerScript>().gravityDown) ? 0 : 180)
            );
	}
}
