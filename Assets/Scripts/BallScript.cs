﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///Script definig the behaviour of the ball
public class BallScript : MonoBehaviour {

    public ControllerScript controllerScript;   //The Controller
    public Rigidbody rb;                        //The Rigidbody of the ball
    public int player = -1;                     //The player currently "owning" the ball
    public float baseSpeed = 10f;               //The base speed of the ball
    public float speed;                         //The speed of the ball, increases when hit, resets to base when scoring
    public float maxSpeed = 30f;                //The maximum speed of the ball

    // Use this for initialization
    void Start ()
    {
        float ranDir = Random.Range(0f, Mathf.PI * 2f);                         //Random radian angle
        rb.velocity = new Vector3(Mathf.Cos(ranDir), Mathf.Sin(ranDir)) * 10f;  //Initial velocity
        speed = baseSpeed;                                                      //Set speed to base
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, 0);    //Sets the z position to be 0

        if (speed > maxSpeed)                           //
            speed = maxSpeed;                           //Limits speed to maxspeed
        rb.velocity = rb.velocity.normalized * speed;   //Set velocity

        //This part checks what the ball is about to hit to avoid it phasing through objects
        RaycastHit rh;  //The object storing information about the raycast
        //Debug
        Debug.DrawLine(transform.position + rb.velocity.normalized * transform.localScale.x, transform.position + rb.velocity * Time.fixedDeltaTime);
        //Raycast from edge of ball
        if (Physics.Raycast(transform.position + rb.velocity.normalized * transform.localScale.x, rb.velocity, out rh, rb.velocity.magnitude * Time.fixedDeltaTime))
        {
            //rb.velocity = rb.velocity.normalized * rh.distance * 1.1f;
            //Move ball onto the point it should hit
            transform.position = rh.point - rb.velocity.normalized * transform.localScale.x * .9f;
        }

        //Sets the trail color to a player's
        if (player != -1)
            GetComponent<TrailRenderer>().material.color = controllerScript.playerColor[player];
    }

    //Colliding with players
    private void OnCollisionEnter(Collision collision)
    {
        PlayerCollision(collision.transform);
    }

    //Colliding with the player's hit
    private void OnTriggerEnter(Collider other)
    {
        PlayerCollision(other.transform.parent);
    }

    //Player Collision
    void PlayerCollision(Transform trans)
    {
        if (trans.name.Contains("Player"))  //if player
        {
            if (trans.GetComponent<PlayerScript>().hitting) //if player is hitting
            {
                player = trans.GetComponent<PlayerScript>().player; //Set owner of ball
                speed *= 1.1f;                                      //Increase speed by 10%
            }
            else if (player != trans.GetComponent<PlayerScript>().player)    //if player isn't hitting and isn't owner
            {
                if (player != -1)           //if ball has an owner
                {
                    controllerScript.playerScore[player]++;                 //Increase score
                    speed = baseSpeed;                                      //Reset speed
                    trans.GetComponent<PlayerScript>().gotHitAS.Play();     //Play hit sound
                    //Time.timeScale = .1f;                                 //Slows time
                    //StartCoroutine(SlowMo(.02f));                         //Sets timer for slowmo
                    trans.GetComponent<PlayerScript>().beenHit = true;      //Set hit invincibility
                    /*foreach (Transform t in trans.GetChild(1).GetChild(0))  //Make player transparent during invincibility
                        t.GetComponent<Renderer>().material.color = new Color(
                        controllerScript.playerColor[trans.GetComponent<PlayerScript>().player].r,
                        controllerScript.playerColor[trans.GetComponent<PlayerScript>().player].g,
                        controllerScript.playerColor[trans.GetComponent<PlayerScript>().player].b,
                        .5f);*/
                    StartCoroutine(trans.GetComponent<PlayerScript>().HitPlayer(.5f));  //Sets timer on invincibility
                }
            }
            //Set new velocity
            rb.velocity += (transform.position - trans.position) * 10f;
        }
    }

    //Collision with the outer ring
    private void OnCollisionExit(Collision collision)
    {
        if (collision.transform.name.Contains("Torus"))
        {
            //Move towards the centre
            rb.velocity -= transform.position - controllerScript.gravityCentre.transform.position/2f;
            //Normalize the velocity
            rb.velocity = rb.velocity.normalized * speed;
        }
    }

    //SlowMo duration
    private IEnumerator SlowMo(float active)
    {
        yield return new WaitForSeconds(active);
        Time.timeScale = 1f;
    }
}
