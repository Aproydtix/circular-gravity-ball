﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///Script to rotate the outer ring around the arena
public class RingScript : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        //Rotates the ring
        transform.RotateAround(transform.position, transform.up, Time.deltaTime * 10f);
	}
}
