﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///Script to rotate the sphere in the middle on the arena
public class SphereScript : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        //Rotates the sphere
        transform.localEulerAngles += new Vector3(0, 0, Time.deltaTime*10f);
    }
}
