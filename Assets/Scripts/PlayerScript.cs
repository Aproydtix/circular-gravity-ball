﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///Script defining the input and behaviour of the player object
public class PlayerScript : MonoBehaviour {

    public ControllerScript controllerScript;   //Controller Script
    public GameObject gravitySource;            //The source of the gravity
    public Rigidbody rb;                        //Rigidbody
    public AudioSource jumpAS;                  //The jump audio source
    public AudioSource hitAS;                   //The hit audio source
    public AudioSource gotHitAS;                //The got hit audio source
    public Animation anim;                      //The animation
    bool runAnim = true;                        //Whether running animation is active

    public int player;                          //The player id
    KeyCode keyRight;                           //The right key
    KeyCode keyLeft;                            //The left key
    KeyCode keyJump;                            //The jump key
    KeyCode keyGravity;                         //The gravity key
    KeyCode keyHit;                             //The hit key
    bool right;                                 //Whether right key is held
    bool left;                                  //Whether left key is held
    bool jump;                                  //Whether jump key is pressed
    bool gravity;                               //Whether gravity key is pressed
    bool hit;                                   //Whether hit key is pressed
    
    bool canHit = true;                         //Whether player can hit
    public float speed;                         //The player speed
    public float maxSpeed = 1f;                 //The player maxspeed
    public float jumpForce = 100f;              //The force uesd for jumping
    public bool gravityDown = true;             //Whether gravity is pointing downwards
    public bool grounded = false;               //Whether the player is touching the ground or not
    public bool hitting = false;                //Whether the player is currently hitting
    Transform hitObject;                        //The hit effect
    public bool beenHit = false;                //Used for hit invincibility

    

    // Use this for initialization
    void Start ()
    {
        //Player 1 input
        if (player == 0)
        {
            keyRight = KeyCode.D;
            keyLeft = KeyCode.A;
            keyJump = KeyCode.W;
            keyGravity = KeyCode.S;
            keyHit = KeyCode.Q;
        }
        //Player 2 input
        if (player == 1)
        {
            keyRight = KeyCode.RightArrow;
            keyLeft = KeyCode.LeftArrow;
            keyJump = KeyCode.UpArrow;
            keyGravity = KeyCode.DownArrow;
            keyHit = KeyCode.RightShift;
        }
        //Player 3 input
        if (player == 2)
        {
            keyRight = KeyCode.H;
            keyLeft = KeyCode.F;
            keyJump = KeyCode.T;
            keyGravity = KeyCode.G;
            keyHit = KeyCode.R;
        }
        //Player 4 input
        if (player == 3)
        {
            keyRight = KeyCode.L;
            keyLeft = KeyCode.J;
            keyJump = KeyCode.I;
            keyGravity = KeyCode.K;
            keyHit = KeyCode.U;
        }

        //Sets the color of the player
        foreach (Transform t in transform.GetChild(1).GetChild(0))
            t.GetComponent<Renderer>().material.color = controllerScript.playerColor[player];
        anim = GetComponent<Animation>();           //Find the animation component
        anim["loop_run_funny"].speed = 4;           //Set animation speed
        hitObject = transform.GetChild(0);          //Find the hit object
        //Set hit object color
        hitObject.GetComponent<Renderer>().material.color = new Color(
            controllerScript.playerColor[player].r,
            controllerScript.playerColor[player].g,
            controllerScript.playerColor[player].b,
            .5f
            );
    }

    // Update is called once per frame
    void Update()
    {
        //Input should be handled in Update
        if (Input.GetKeyDown(keyGravity))
            gravity = true;
        if (Input.GetKeyDown(keyHit))
            hit = true;
        if (Input.GetKeyDown(keyJump))
            jump = true;
        right = Input.GetKey(keyRight);
        left = Input.GetKey(keyLeft);
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        //Sets z position to 0
        transform.position = new Vector3(transform.position.x, transform.position.y, 0);

        if (gravity)                                        //if gravity key pressed
        {
            gravityDown = !gravityDown;                     //Reverse gravity
            grounded = false;                               //Set grounded to false
            gravity = false;                                //Set gravity key pressed to false
            transform.eulerAngles += new Vector3(0, 0, 180);//Rotate object 180 degrees
            transform.position -= transform.up;             //Move object by arbitrary amount to counter a bug
        }

        if (hit)                                        //if hit key pressed
        {
            if (canHit)                                 //if player can hit
            {
                anim.CrossFade("punch_hi_left");        //Change animation
                runAnim = false;                        //Stop running animation
                StartCoroutine(anima(0.5f));            //Start animation timer
                hitAS.Play();                           //Play hit sound
                hitting = true;                         //Player is now hitting
                hitObject.gameObject.SetActive(true);   //Set the hit object to active
                StartCoroutine(HittingBall(.5f));       //Start timer for hit object
                canHit = false;                         //Player can no longer hit
                StartCoroutine(HitCD(1f));              //Sets cooldown on hitting
            }
            hit = false;                                //Set hit key pressed to false
        }

        //Sets gravity direction
        Vector3 gravDir = (gravitySource.transform.position - transform.position).normalized * ((gravityDown) ? 1 : -1);
        //Add force towards gravity source
        rb.AddForce(gravDir * controllerScript.gravityIntensity);

        //Rotates the player towards the gravity source
        Quaternion newDir = Quaternion.FromToRotation(transform.up, -gravDir) * transform.rotation;
        transform.rotation = newDir;
        //Debugs various axis
        Debug.DrawLine(transform.position, transform.position + gravDir);
        Debug.DrawLine(transform.position, transform.position + transform.up, Color.green);
        Debug.DrawLine(transform.position, transform.position + transform.right, Color.red);
        Debug.DrawLine(transform.position, transform.position + transform.forward, Color.blue);

        if (jump)                                   //if jump key pressed
        {
            if (grounded)                           //if player is grounded
            {
                anim.Play("jump");                  //Play jump animation
                runAnim = false;                    //Disable running animation
                jumpAS.Play();                      //Play jump sound
                rb.AddForce(-gravDir * jumpForce);  //Add force against gravity direction
                grounded = false;                   //Player is no longer grounded
                StartCoroutine(anima(0.5f));        //Set animation timer
            }
            jump = false;                           //Set jump key pressed to false
        }
        if (right)                                  //Move right
            rb.velocity += transform.right * speed * ((gravityDown) ? 1 : -1);
        if (left)                                   //Move left
            rb.velocity -= transform.right * speed * ((gravityDown) ? 1 : -1);
        if (right && !left)                         //Makes player look right
            transform.localScale = new Vector3(-.75f * ((gravityDown) ? 1 : -1), .75f, .75f);
        if (left && !right)                         //Makes player look left
            transform.localScale = new Vector3(.75f * ((gravityDown) ? 1 : -1), .75f, .75f);

        //Vector transforming the players velocity to be relative to the transform rotation
        Vector3 localVelocity = transform.InverseTransformDirection(rb.velocity);

        if ((!right && !left) || (right && left))   //if not moving
            localVelocity.x /= 1.5f;                //Reduce velocity across x-axis

        if (Mathf.Abs(localVelocity.x) > maxSpeed)  //If speed over maxspeed on x-axis
            localVelocity.x = maxSpeed * Mathf.Sign(localVelocity.x);   //Set speed to maxspeed

        //Run animation
        if ((localVelocity.x > 0.9 || localVelocity.x < -0.9) && runAnim)
        {
            anim.CrossFade("loop_run_funny");
        }
        else if (runAnim) //Transition into idle animation
        {
            anim.CrossFade("loop_idle");
        }

        //Transform the velocity back to world and set it
        rb.velocity = transform.TransformDirection(localVelocity);
    }

    //Timer for hit object
    private IEnumerator HittingBall(float active)
    {
        yield return new WaitForSeconds(active);
        hitting = false;                        //No longer hitting
        hitObject.gameObject.SetActive(false);  //Deactivate object
    }

    //Timer for hit cooldown
    private IEnumerator HitCD(float active)
    {
        yield return new WaitForSeconds(active);
        canHit = true;                          //Player can hit again
    }

    //Timer fo hit invincibility
    public IEnumerator HitPlayer(float active)
    {
        yield return new WaitForSeconds(active);
        beenHit = false;                                            //Player is no longer hit
        /*foreach (Transform t in transform.GetChild(1).GetChild(0))  //Set color back to opaque
            t.GetComponent<Renderer>().material.color = controllerScript.playerColor[player];*/
    }
    
    //Animation timer
    private IEnumerator anima(float active)
    {
        yield return new WaitForSeconds(active);
        runAnim = true;
    }

    //Collision with ground
    private void OnCollisionEnter(Collision collision)
    {
        if ((collision.transform.name.Contains("Inner") && gravityDown)         //Collision with inner ring
            || (collision.transform.name.Contains("Torus") && !gravityDown))    //Collision with outer ring
        {
            grounded = true;    //Player is currently grounded
        }
    }
}
