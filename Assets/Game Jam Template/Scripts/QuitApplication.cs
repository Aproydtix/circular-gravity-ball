﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class QuitApplication : MonoBehaviour {

	public void Quit()
	{
        //If we are running in a standalone build of the game
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByBuildIndex(0))
        {
            #if UNITY_STANDALONE
                        //Quit the application
                        Application.Quit();
            #endif
            
                        //If we are running in the editor
            #if UNITY_EDITOR
                        //Stop playing the scene
                        UnityEditor.EditorApplication.isPlaying = false;
            #endif
        }
        else
        {
            GameObject.Find("UI").GetComponent<Pause>().UnPause();
            Destroy(GameObject.Find("UI"));
            SceneManager.LoadScene(0);
        }
	}
}
