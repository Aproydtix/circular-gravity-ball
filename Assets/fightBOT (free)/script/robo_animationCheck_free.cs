﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;
using UnityEngine.Animations;

[ExecuteInEditMode]
public class robo_animationCheck_free : MonoBehaviour
{

    private Animation anim;

    private string[] loops = { "loop_idle", "loop_run_funny", "loop_walk_funny" };
    private string[] combos = { "cmb_street_fight" };
    private string[] kick = { "kick_jump_right", "kick_lo_right" };
    private string[] punch = { "punch_hi_left", "punch_hi_right" };
    private string[] rest = { "def_head", "final_head", "jump", "xhit_body", "xhit_head" };





    void Start()
    {
        anim = GetComponent<Animation>();

        anim["loop_run_funny"].speed = 4.0f;
        // anim["kick_hi_left"].speed=1.6f;
        // anim["kick_jump_spin"].speed=1.6f;
    }




    void OnGUI()
    {

        CategoryView("loops", loops, 10);
        CategoryView("combos", combos, 170);
        CategoryView("kicks", kick, 330);
        CategoryView("punches", punch, 490);
        CategoryView("the rest", rest, 650);
    }




    void CategoryView(string nme, string[] cat, int x)
    {
        GUI.Box(new Rect(x, 10, 150, 25), nme);
        for (int i = 0; i < cat.Length; i++)
        {
            if (GUI.Button(new Rect(x, 35 + (25 * i), 150, 25), cat[i]))
            {

                GoAnim(cat[i]);
            }
        }

    }



    IEnumerator GoAnim(string nme)
    {

        anim.CrossFade(nme);

        while (anim.isPlaying)
        {
            // do nothing
            yield return 0;
        }

        anim.CrossFade("loop_idle");
    }
}